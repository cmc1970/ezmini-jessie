#  FROM debian:stable-slim 
#  FROM debian:bullseye-slim 
#  FROM debian:stretch-slim 
#  FROM cmch/ezmini-jessie:pcsc
#  FROM cmch/ezmini-jessie:ezmini
   FROM cmch/ezmini-jessie:ezmini-stretch


ARG DEBIAN_FRONTEND=noninteractive


RUN \
    u=https://api-hisecurecdn.cdn.hinet.net/HiPKILocalSignServer/linux/HiPKILocalSignServerApp.tar.gz && \
    wget -O /dev/stdout $u --no-check-certificate | tar zxvf - -C /usr/local
